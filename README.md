# Prototype 

Prototype of what happens internally at Nickson after a new customer successfully checks out.

## Video Walkthrough

Here's a walkthrough of different animations:

![ ](chair_walkthrough.gif)

### Chair Userflow Setps
1. Once you open app, login screen automatically goes into facetime recognition in less than a second
2. _Activity screen_, tap completed
3. tap completed jobs to scroll north (no south scrolling yet)
4. tap _Bar Chart_ icon
5. Insights screen, tap graphs to scroll north
6. tap _Back_ button to return to Activity
7. tap pending design _cell_
8. Customer screen, tap _AI Design_ button
9. _"Thinking"_ screen will appear and dismiss itself
10. tap _floor plan_ to scroll east (suppose to represent going from entire floor plan to a by room floor plan)
11. tap _Edit_ button
12. You dislike chair so tap _chair (living room)_ to swap it out
13. after _chair_ is selected, tap _Swap_ button (top right corner)
14. tap any chair rectangles to scroll north (no south scrolling yet)
15. tap bottom right _Gray chair_ rectangle
16. tap _Confirm_ on top right
17. Design screen - See chair has been swapped out
18. tap _Continue_ button
19. _“Thinking”_ screen appears - it will dismisses itself
20. Move screen, tap _Add to Calendars_ button - screen dismisses itself
21. **Final Screen** - See calendar event on locked iPhone

### Nightstand Userflow Setps
1. Once you open app, login screen automatically goes into facetime recognition in less than a second
2. _Activity screen_, tap completed
3. tap completed jobs to scroll north (no south scrolling yet)
4. tap _Bar Chart_ icon
5. Insights screen, tap graphs to scroll north
6. tap _Back_ button to return to Activity
7. tap pending design _cell_
8. Customer screen, tap _AI Design_ button
9. _“Thinking”_ screen appears - it will dismisses itself
11. tap _floor plan_ to scroll east (suppose to represent going from entire floor plan to a by room floor plan)
12. tap _Edit_ button
13. You dislike nightstand, so tap _nightstand_ cell in order to highlight
14. tap _Swap_ on top right
15. tap the _white nightstand_ to select
16. tap _Confirm_ on top right
17. Design screen - See chair has been swapped out
18. tap _Continue_ button
19. _“Thinking”_ screen appears - it will dismisses itself
20. Move screen, tap _Add to Calendars_ button - screen dismisses itself
21. **Final Screen** - See calendar event on locked iPhone